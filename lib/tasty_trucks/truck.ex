defmodule TastyTrucks.Truck do
  @moduledoc """

  """
  defstruct [
    :name,
    :status,
    :food_items,
    :address,
    :block,
    :lot,
    :coordinates
  ]
end
