defmodule TastyTrucks do
  @moduledoc """
  Documentation for `TastyTrucks`.
  """

  alias CSV.Decoding.Decoder

  alias TastyTrucks.Truck

  @data_file_path Path.expand("./example_data_files/data.csv", __DIR__)

  def main(_args \\ []) do
    show_intro_prompts_and_get_craving()
    |> get_approved_and_matching_trucks()
    |> Enum.each(&IO.puts/1)
  end

  # This function was made small and with minimal side effects, to make it
  # suitable for testing the final result of the data manipulation pipeline.
  def get_approved_and_matching_trucks(craving) do
    parse_and_extract_trucks()
    |> map_to_structs()
    |> filter_trucks(craving)
    |> prepare_for_output()
    |> Enum.to_list()
  end

  def parse_and_extract_trucks do
    @data_file_path
    |> File.stream!()
    |> Decoder.decode(headers: true)
    |> Stream.map(fn
      {:ok, truck} -> truck
      _ -> nil
    end)
    |> Stream.reject(&is_nil/1)
  end

  def show_intro_prompts_and_get_craving do
    IO.puts("Welcome to the Tasty Trucks app!")
    IO.puts("What are you craving?")
    IO.puts("(You can search with partial words and incomplete phrases)")
    IO.puts("(And no need to worry about capitalization)")
    IO.puts("(examples: \"taco\", \"TaCos\", \"hot dog\", \"t doG\")")

    craving =
      IO.gets("")
      |> String.downcase()
      |> String.trim()

    IO.puts("Here are some places serving #{craving}:\n\n")
    IO.puts("========================================\n")

    craving
  end

  def map_to_structs(trucks) do
    Stream.map(trucks, fn truck ->
      # destructuring like this means more keystrokes, but also less noise when
      # building the struct.
      %{
        "Applicant" => name,
        "Status" => status,
        "FoodItems" => food_items,
        "Address" => address,
        "block" => block,
        "lot" => lot,
        "Location" => coordinates
      } = truck

      %Truck{
        name: name,
        status: status,
        food_items: food_items,
        address: address,
        block: block,
        lot: lot,
        coordinates: coordinates
      }
    end)
  end

  def filter_trucks(trucks, craving) do
    trucks
    |> Stream.filter(fn %Truck{status: status} -> status == "APPROVED" end)
    |> Stream.filter(fn %Truck{food_items: food_items} ->
      food_items
      |> String.downcase()
      |> String.contains?(craving)
    end)
  end

  def prepare_for_output(trucks) do
    Stream.map(trucks, fn truck ->
      """
      #{String.capitalize(truck.name)}
      ---------------------------------
      Location:
      #{truck.address}
      (Block #{truck.block}, Lot #{truck.lot})
      #{check_for_coordinates(truck)}
      """
    end)
  end

  # Some trucks have coordinates (0.0, 0.0), which would be Null Island rather
  # than somewhere in SF (all of which is on Latitude 37.* degrees North)
  def check_for_coordinates(%Truck{coordinates: coordinates}) do
    coordinates
    # skip the first paren
    |> String.slice(1, 4)
    |> Integer.parse()
    |> case do
      {0, _remainder} -> ""
      _ -> coordinates
    end
  end
end
