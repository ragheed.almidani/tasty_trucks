# TastyTrucks

This program asks the user for a food craving, and finds food trucks in San
Francisco which serve the item.

The program is written in Elixir.

## Running the program

For convenience, I have already built an executable using
[escript](https://hexdocs.pm/mix/1.15.5/Mix.Tasks.Escript.Build.html), which can
run on a machine with Erlang/OTP installed without needing to fetch
dependencies, compile, etc.

Just change to the directory on your GNU/Linux or MacOS computer and run:

`./tasty_trucks`

## Things I learned

I gained a greater appreciation for well-named functions. I started with a long
pipeline of transformations in one function, but I realized breaking these steps
into smaller, more focused functions would make it easier for someone glancing
at the top of the main file to understand the program at a high level.

I also used escript for the first time, which seems like a sensible way to
share programs with other developers without requiring they install a specific
version of Elixir. Combined with the clear mapping to a `:main_module` with a
conventional entrypoint, this helped me appreciate escript a a seamless way to
write, run, and share an Elixir program!

## What is missing?

I began to break out a more "pure" function which would have facilitated proper
testing, but ran out of time to actually write tests.

I also wanted to include more comments. This is somewhat made up for by the
naming of the functions and overall organization of the program.

## Final thoughts

Thank you for this opportunity to learn and showcase my programming knowledge
and Elixir skills, and for considering me to join your team. I look forward to
your feedback, and hopefully next steps!

